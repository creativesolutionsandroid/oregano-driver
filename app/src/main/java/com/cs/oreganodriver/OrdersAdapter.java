package com.cs.oreganodriver;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by CS on 18-08-2016.
 */
public class OrdersAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Order> favOrderList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    //public ImageLoader imageLoader;

    public OrdersAdapter(Context context, ArrayList<Order> favOrderList) {
        this.context = context;
        this.favOrderList = favOrderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return favOrderList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView storeName, customerAddress, totalPrice, itemDetails, expectedTime, invoiceNumber;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.order_listitem, null);


            holder.storeName = (TextView) convertView
                    .findViewById(R.id.store_name);
            holder.customerAddress = (TextView) convertView
                    .findViewById(R.id.customer_address);
            holder.totalPrice = (TextView) convertView
                    .findViewById(R.id.price);
            holder.invoiceNumber = (TextView) convertView
                    .findViewById(R.id.invoce_number);

//            holder.itemDetails = (TextView) convertView.findViewById(R.id.item_details);
            holder.expectedTime = (TextView) convertView.findViewById(R.id.expected_time);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.storeName.setText(favOrderList.get(position).getStoreName());
        Log.i("TAG", "store name " + favOrderList.get(position).getStoreName());
        holder.customerAddress.setText("Deliver at " + favOrderList.get(position).getAddress());
        holder.invoiceNumber.setText("#" + favOrderList.get(position).getInvoiceNo());

        SimpleDateFormat date1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        SimpleDateFormat date3 = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        Date date2 = null, time2 = null;

        try {
            time2 = date1.parse(favOrderList.get(position).getExpectedTime());
        } catch (Exception e1) {
            e1.printStackTrace();
        }


    String date4 = date3.format(time2);
        holder.expectedTime.setText(date4);
//        holder.expectedTime.setText(favOrderList.get(position).getExpectedTime());
         holder.totalPrice.setText(favOrderList.get(position).getTotalPrice()+" SR");


        return convertView;
}
}