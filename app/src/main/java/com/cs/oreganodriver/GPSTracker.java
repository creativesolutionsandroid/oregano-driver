package com.cs.oreganodriver;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Locale;

public class GPSTracker extends Service implements LocationListener {
//	private final Context mContext;

	// flag for GPS Status
	boolean isGPSEnabled = false;

	// flag for network status
	boolean isNetworkEnabled = false;

	boolean canGetLocation = false;

	Location location;
	double latitude;
	double longitude;

	// The minimum distance to change updates in metters
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 10
																	// metters

	// The minimum time beetwen updates in milliseconds
	private static final long MIN_TIME_BW_UPDATES = 1000 * 10 * 1; // 1 minute

	// Declaring a Location Manager
	protected LocationManager locationManager;

	private static final String PACKAGE_NAME =
			"com.cs.oreganodriver";

	private static final String TAG = "TAG";

	/**
	 * The name of the channel for notifications.
	 */
	private static final String CHANNEL_ID = "channel_01";

	static final String ACTION_BROADCAST = PACKAGE_NAME + ".broadcast";

	static final String EXTRA_LOCATION = PACKAGE_NAME + ".location";
	private static final String EXTRA_STARTED_FROM_NOTIFICATION = PACKAGE_NAME +
			".started_from_notification";

	private final IBinder mBinder = new LocalBinder();

	/**
	 * The identifier for the notification displayed for the foreground service.
	 */
	private static final int NOTIFICATION_ID = 12345678;

	/**
	 * Used to check whether the bound activity has really gone away and not unbound as part of an
	 * orientation change. We create a foreground service notification only if the former takes
	 * place.
	 */
	private boolean mChangingConfiguration = false;

	private NotificationManager mNotificationManager;

	private Handler mServiceHandler;

	/**
	 * The current location.
	 */
	private Location mLocation;

	String userId;

	SharedPreferences userPrefs;
	SharedPreferences.Editor userPrefEditor;

//	public GPSTracker(Context context) {
//		this.mContext = context;
//		getLocation();
//	}

	public GPSTracker() {

	}

	@Override
	public void onCreate() {
		userPrefs = this.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
		userPrefEditor = userPrefs.edit();
		userId = userPrefs.getString("userId", null);

		getLocation();

		HandlerThread handlerThread = new HandlerThread(TAG);
		handlerThread.start();
		mServiceHandler = new Handler(handlerThread.getLooper());
		mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		// Android O requires a Notification Channel.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			CharSequence name = getString(R.string.app_name);
			// Create the channel for the notification
			NotificationChannel mChannel =
					new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);

			// Set the Notification Channel for the Notification Manager.
			mNotificationManager.createNotificationChannel(mChannel);
		}
	}

	public Location getLocation() {
		Utils.setRequestingLocationUpdates(this, true);
		try {
			startService(new Intent(getApplicationContext(), GPSTracker.class));

			locationManager = (LocationManager) getApplicationContext()
					.getSystemService(LOCATION_SERVICE);

			// getting GPS status
			isGPSEnabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);

			// getting network status
			isNetworkEnabled = locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if (!isGPSEnabled && !isNetworkEnabled) {
				// no network provider is enabled
			} else {
				this.canGetLocation = true;

				if (isGPSEnabled) {
					if (location == null) {
						locationManager.requestLocationUpdates(
								LocationManager.GPS_PROVIDER,
								MIN_TIME_BW_UPDATES,
								MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

						Log.d("GPS Enabled",
								"========================GPS Enabled");

						if (locationManager != null) {
							location = locationManager
									.getLastKnownLocation(LocationManager.GPS_PROVIDER);
							updateGPSCoordinates();
						}
					}
				}

				// First get location from Network Provider
				if (null == location && isNetworkEnabled) {
					locationManager.requestLocationUpdates(
							LocationManager.NETWORK_PROVIDER,
							MIN_TIME_BW_UPDATES,
							MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

					Log.d("Network", "====================Network");

					if (locationManager != null) {
						location = locationManager
								.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						updateGPSCoordinates();
					}
				}

				// if GPS Enabled get lat/long using GPS Services

			}
		} catch (Exception e) {
			// e.printStackTrace();
			Log.e("TAG",
					"Impossible to connect to LocationManager", e);
		}

		return location;
	}

	public void updateGPSCoordinates() {
		if (location != null) {
			latitude = location.getLatitude();
			longitude = location.getLongitude();
			Log.d("TAG", "========================GPS Enabled"
					+ latitude);

            JSONObject parent = new JSONObject();
            try {
                JSONArray mainItem = new JSONArray();

                JSONObject mainObj = new JSONObject();
                mainObj.put("Latitude", location.getLatitude());
                mainObj.put("Longitude", location.getLongitude());
                mainObj.put("DriverId", userId);
                mainObj.put("IsActive", true);
                mainObj.put("OnlineStatus", true);

                mainItem.put(mainObj);

                parent.put("DriverLoc", mainItem);
                Log.d(TAG, "JSON: "+parent.toString());
            } catch (JSONException je) {
                je.printStackTrace();
            }

            new UpdateLocationToServer().execute(parent.toString());
		}
	}

	/**
	 * Stop using GPS listener Calling this function will stop using GPS in your
	 * app
	 */

	public void stopUsingGPS() {
		if (locationManager != null) {
			Utils.setRequestingLocationUpdates(this, true);
			locationManager.removeUpdates(GPSTracker.this);
			stopSelf();
		}
	}

	/**
	 * Function to get latitude
	 */
	public double getLatitude() {
		if (location != null) {
			latitude = location.getLatitude();
		}

		System.out.println(latitude);
		return latitude;
	}

	/**
	 * Function to get longitude
	 */
	public double getLongitude() {
		if (location != null) {
			longitude = location.getLongitude();
		}

		return longitude;
	}

	/**
	 * Function to check GPS/wifi enabled
	 */
	public boolean canGetLocation() {
		return this.canGetLocation;
	}

	/**
	 * Function to show settings alert dialog
	 */
	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(GPSTracker.this);

		// Setting Dialog Title
		alertDialog.setTitle("GPS");

		// Setting Dialog Message
		alertDialog.setMessage("GPS Alert");

		// On Pressing Setting button
		alertDialog.setPositiveButton("ok",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						startActivity(intent);
					}
				});

		// On pressing cancel button
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		alertDialog.show();
	}

	@Override
	public void onLocationChanged(Location location) {
		Log.i(TAG, "New location: " + location);

		mLocation = location;

		// Notify anyone listening for broadcasts about the new location.
//		Intent intent = new Intent(ACTION_BROADCAST);
//		intent.putExtra(EXTRA_LOCATION, location);
//		LocalBroadcastManager.getInstance(GPSTracker.this).sendBroadcast(intent);

		// Update notification content if running as a foreground service.
		if (serviceIsRunningInForeground(getApplicationContext())) {
			mNotificationManager.notify(NOTIFICATION_ID, getNotification());
		}

		JSONObject parent = new JSONObject();
		try {
			JSONArray mainItem = new JSONArray();

			JSONObject mainObj = new JSONObject();
			mainObj.put("Latitude", location.getLatitude());
			mainObj.put("Longitude", location.getLongitude());
			mainObj.put("DriverId", userId);
			mainObj.put("IsActive", true);
			mainObj.put("OnlineStatus", true);

			mainItem.put(mainObj);

			parent.put("DriverLoc", mainItem);
			Log.d(TAG, "JSON: "+parent.toString());
		} catch (JSONException je) {
			je.printStackTrace();
		}

		new UpdateLocationToServer().execute(parent.toString());
	}

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.i(TAG, "in onBind()");
		stopForeground(true);
		mChangingConfiguration = false;
		return mBinder;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i(TAG, "Service started");
		boolean startedFromNotification = intent.getBooleanExtra(EXTRA_STARTED_FROM_NOTIFICATION,
				false);

		// We got here because the user decided to remove location updates from the notification.
		if (startedFromNotification) {
			stopUsingGPS();
			stopSelf();
		}
		// Tells the system to not try to recreate the service after it has been killed.
		return START_NOT_STICKY;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mChangingConfiguration = true;
	}

	@Override
	public void onRebind(Intent intent) {
		// Called when a client (MainActivity in case of this sample) returns to the foreground
		// and binds once again with this service. The service should cease to be a foreground
		// service when that happens.
		Log.i(TAG, "in onRebind()");
		stopForeground(true);
		mChangingConfiguration = false;
		super.onRebind(intent);
	}

	@Override
	public boolean onUnbind(Intent intent) {
		Log.i(TAG, "Last client unbound from service");

		// Called when the last client (MainActivity in case of this sample) unbinds from this
		// service. If this method is called due to a configuration change in MainActivity, we
		// do nothing. Otherwise, we make this service a foreground service.
		if (!mChangingConfiguration && Utils.requestingLocationUpdates(this)) {
			Log.i(TAG, "Starting foreground service");

			startForeground(NOTIFICATION_ID, getNotification());
		}
		return true; // Ensures onRebind() is called when a client re-binds.
	}

	/**
	 * Returns the {@link NotificationCompat} used as part of the foreground service.
	 */
	private Notification getNotification() {
		Intent intent = new Intent(this, GPSTracker.class);

		CharSequence text = Utils.getLocationText(mLocation);

		// Extra to help us figure out if we arrived in onStartCommand via the notification or not.
		intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true);

		// The PendingIntent that leads to a call to onStartCommand() in this service.
		PendingIntent servicePendingIntent = PendingIntent.getService(this, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);

		// The PendingIntent to launch activity.
		PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0,
				new Intent(this, MainActivity.class), 0);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
				.addAction(R.drawable.app_logo, getString(R.string.launch_activity),
						activityPendingIntent)
				.setContentText(text)
				.setContentTitle(Utils.getLocationTitle(this))
				.setOngoing(true)
				.setPriority(Notification.PRIORITY_HIGH)
				.setSmallIcon(R.drawable.app_logo)
				.setSound(null)
				.setTicker(text)
				.setWhen(System.currentTimeMillis());

		// Set the Channel ID for Android O.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			builder.setChannelId(CHANNEL_ID); // Channel ID
		}

		return builder.build();
	}

	@Override
	public void onDestroy() {
		mServiceHandler.removeCallbacksAndMessages(null);
	}


	public class LocalBinder extends Binder {
		GPSTracker getService() {
			return GPSTracker.this;
		}
	}

	/**
	 * Returns true if this is a foreground service.
	 *
	 * @param context The {@link Context}.
	 */
	public boolean serviceIsRunningInForeground(Context context) {
		ActivityManager manager = (ActivityManager) context.getSystemService(
				Context.ACTIVITY_SERVICE);
		for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(
				Integer.MAX_VALUE)) {
			if (getClass().getName().equals(service.service.getClassName())) {
				if (service.foreground) {
					return true;
				}
			}
		}
		return false;
	}

	public class UpdateLocationToServer extends AsyncTask<String, Integer, String> {

		ProgressDialog pDialog;
		//        ProgressDialog dialog;
		String response;
		InputStream inputStream = null;

		@Override
		protected void onPreExecute() {
//            dialog = ProgressDialog.show(MainActivity.this, "",
//                    "Registering...");
			Log.d(TAG, "onPreExecute: ");
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				try {
					// 1. create HttpClient
					HttpClient httpclient = new DefaultHttpClient();

					// 2. make POST request to the given URL
					HttpPost httpPost = new HttpPost(Constants.UPDATE_LOCATION);


					// ** Alternative way to convert Person object to JSON string usin Jackson Lib
					// ObjectMapper mapper = new ObjectMapper();
					// json = mapper.writeValueAsString(person);

					// 5. set json to StringEntity
					StringEntity se = new StringEntity(params[0], "UTF-8");

					// 6. set httpPost Entity
					httpPost.setEntity(se);

					// 7. Set some headers to inform server about the type of the content
					httpPost.setHeader("Accept", "application/json");
					httpPost.setHeader("Content-type", "application/json");

					// 8. Execute POST request to the given URL
					HttpResponse httpResponse = httpclient.execute(httpPost);

					// 9. receive response as inputStream
					inputStream = httpResponse.getEntity().getContent();

					// 10. convert inputstream to string
					if (inputStream != null) {
						response = convertInputStreamToString(inputStream);
						return response;
					}

				} catch (Exception e) {
					Log.d(TAG, e.getLocalizedMessage());
				}
			} catch (Exception e) {
				e.printStackTrace();
				Log.e(TAG, "Error converting result " + e.toString());
			}
			Log.i(TAG, "user response:" + response);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			Log.d(TAG, "onPostExecute: "+response);
			if (result != null) {
				if (result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(MainActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

				} else {
					if (result.equals("")) {
						Toast.makeText(GPSTracker.this, "cannot reach server", Toast.LENGTH_SHORT).show();
					} else {
						try {
							JSONObject jo = new JSONObject(result);

						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}

			} else {
//                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
			}
//            if(dialog != null) {
//                dialog.dismiss();
//            }

			super.onPostExecute(result);

		}

	}

	private static String convertInputStreamToString(InputStream inputStream) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;

	}
}
