package com.cs.oreganodriver;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;

import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    public static TabsPagerAdapter mAdapter;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    boolean loading = false;


    private Timer timer = new Timer();

    String userId;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String response;
    String mLoginStatus;

    ArrayList<Order> ordersList = new ArrayList<>();

    GPSTracker gps;
    public static Double lat, longi;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;

    ImageView edit;

    private GPSTracker mService;

    // Tracks the bound state of the service.
    private boolean mBound = false;

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            GPSTracker.LocalBinder binder = (GPSTracker.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);

        edit = (ImageView) findViewById(R.id.edit);
        mViewPager.setAdapter(mAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);
        mLoginStatus = userPrefs.getString("login_status", "");

//        new GetOrderDetails().execute(Constants.live_url + "/api/DriverApp?DriverId=" + userId);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(MainActivity.this, EditProfile.class);
                startActivity(a);
            }
        });

//        if(response != null) {
//            try {
//                JSONObject property = new JSONObject(response);
//                JSONObject userObjuect = property.getJSONObject("profile");
//
//                if(userObjuect.getString("nick_name").length()>0) {
//                    getSupportActionBar().setTitle(userObjuect.getString("nick_name"));
//                }else{
//                    getSupportActionBar().setTitle(userObjuect.getString("fullName"));
//                }
//            } catch (JSONException e) {
//                Log.d("", "Error while parsing the results!");
//                e.printStackTrace();
//            }
//        }

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

//        timer.schedule(new MyTimerTask(), 2000, 60000);

//        Log.i("TAG","size "+ordersList.get(0).expectedTime);
    }

    /**
     * Receiver for broadcasts sent by {@link LocationUpdatesService}.
     */
    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(LocationUpdatesService.EXTRA_LOCATION);
            Log.d("TAG", "onReceive: "+location);
            if (location != null) {
                sendLocation(location);
            }
        }
    }


    private void sendLocation(Location location){
        JSONObject parent = new JSONObject();
        try {
            JSONArray mainItem = new JSONArray();

            JSONObject mainObj = new JSONObject();
            mainObj.put("Latitude", location.getLatitude());
            mainObj.put("Longitude", location.getLongitude());
            mainObj.put("DriverId", userId);
            mainObj.put("IsActive", true);
            mainObj.put("OnlineStatus", true);

            mainItem.put(mainObj);

            parent.put("DriverLoc", mainItem);
        } catch (JSONException je) {
            je.printStackTrace();
        }

        new UpdateLocationToServer().execute(parent.toString());
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void getGPSCoordinates() {
        gps = new GPSTracker();
        JSONObject parent = new JSONObject();
        if (gps != null) {
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
//                lat = 24.805712;
//                longi = 46.693132;
                // Create a LatLng object for the current location
//                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                listView.setAdapter(mAdapter);
//                lat = 17.418777;
//                longi = 78.435455;
                try {


                    JSONArray mainItem = new JSONArray();

                    JSONObject mainObj = new JSONObject();
                    mainObj.put("Latitude", lat);
                    mainObj.put("Longitude", longi);
                    mainObj.put("DriverId", userId);
                    mainObj.put("IsActive", true);
                    mainObj.put("OnlineStatus", true);

                    mainItem.put(mainObj);

                    parent.put("DriverLoc", mainItem);
                } catch (JSONException je) {
                    je.printStackTrace();
                }
//                new UpdateLocationToServer().execute("http://85.194.94.241/oreganoservices/api/DriverApp?Latitude="+lat+"&Longitude="+longi+"&driverid="+userId);

                new UpdateLocationToServer().execute(parent.toString());
                Log.i("parent TAG", "" + parent.toString());
                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(MainActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    bindService(new Intent(this, GPSTracker.class), mServiceConnection,
                            Context.BIND_AUTO_CREATE);
                } else {
                    Toast.makeText(MainActivity.this, "Location permission denied", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.action_settings:
                new GetOrderDetails().execute(Constants.live_url + "/api/DriverApp?DriverId=" + userId);

                Log.i("TAG", "size " + ordersList.size());
//                Log.i("TAG", "size " + ordersList.get(0).expectedTime);
//                if (ordersList.size() == 0) {
//                    item.setChecked(true);
//                    JSONObject parent = new JSONObject();
//                    try {
//
//                        JSONArray mainItem = new JSONArray();
//
//                        JSONObject mainObj = new JSONObject();
//                        mainObj.put("Latitude", lat);
//                        mainObj.put("Longitude", longi);
//                        mainObj.put("DriverId", userId);
//                        mainObj.put("IsActive", true);
//                        mainObj.put("OnlineStatus", false);
//
//                        mainItem.put(mainObj);
//
//                        parent.put("DriverLoc", mainItem);
//                    } catch (JSONException je) {
//                        je.printStackTrace();
//                    }
//
////                new UpdateLocationToServer().execute("http://85.194.94.241/oreganoservices/api/DriverApp?Latitude="+lat+"&Longitude="+longi+"&driverid="+userId);
//
//                    new UpdateLocationToServer().execute(parent.toString());
//                    Log.i("parent TAG", "" + parent.toString());
//                    timer.cancel();
//                    userPrefEditor.clear();
//                    userPrefEditor.commit();
//
//                    Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
//                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(loginIntent);
//                    finish();
//                } else {
//                    item.setChecked(false);
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//                    // set title
//                    alertDialogBuilder.setTitle("Oregano");
//
//                    // set dialog message
//                    alertDialogBuilder
//                            .setMessage("Please Complete Your Orders first")
//                            .setCancelable(false)
//                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.dismiss();
//                                }
//                            });
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
                return false;
//                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    //noinspection SimplifiableIfStatement

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }


    public class UpdateLocationToServer extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
//        ProgressDialog dialog;

        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
//            dialog = ProgressDialog.show(MainActivity.this, "",
//                    "Registering...");


        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.live_url + "/api/DriverApp");


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(MainActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
//                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
//            if(dialog != null) {
//                dialog.dismiss();
//            }

            super.onPostExecute(result);

        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public class GetOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {
            ordersList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
            dialog = ProgressDialog.show(MainActivity.this, "",
                    "Loading...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response1:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(MainActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i < ja.length(); i++) {

                                    Order oh = new Order();
                                    JSONObject jo1 = ja.getJSONObject(i);


                                    oh.setOrderId(jo1.getString("orderId"));
                                    oh.setStoreId(jo1.getString("storeId"));
                                    oh.setStoreName(jo1.getString("StoreName"));
                                    oh.setStoreAddress(jo1.getString("StoreAddress"));
                                    oh.setUserId(jo1.getString("UserId"));

                                    oh.setUserMobile(jo1.getString("usrMobile"));
                                    oh.setStoreMobile(jo1.getString("StrPhone"));
                                    oh.setUserLat(jo1.getString("usrLatitude"));
                                    oh.setUserLong(jo1.getString("usrLongitude"));
                                    oh.setStoreLat(jo1.getString("strLatitude"));
                                    oh.setStoreLong(jo1.getString("strLongitude"));
                                    oh.setComments(jo1.getString("comments"));
                                    oh.setOrderDate(jo1.getString("orderDate"));
                                    oh.setExpectedTime(jo1.getString("ExpectedTime"));

                                    oh.setFullName(jo1.getString("FullName"));
                                    oh.setAddress(jo1.getString("Address"));
                                    oh.setHouseNo(jo1.getString("houseno"));
                                    oh.setLandmark(jo1.getString("landmark"));
                                    oh.setTotalPrice(jo1.getString("Total_Price"));
                                    oh.setPaymentMode(jo1.getString("PaymentMode"));
                                    oh.setInvoiceNo(jo1.getString("InvoiceNo"));
                                    oh.setOrderStatus(jo1.getString("OrderStatus"));

                                    if (jo1.getString("OrderStatus").equalsIgnoreCase("Pending")) {
                                        ordersList.add(oh);
                                    }

                                    ordersList.add(oh);
                                    Log.i("TAG","order size "+ordersList.size());

                                }

                            } catch (JSONException je) {
                                je.printStackTrace();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }

            if (ordersList.size() == 0) {
//                                    item.setChecked(true);
                JSONObject parent = new JSONObject();
                try {

                    JSONArray mainItem = new JSONArray();

                    JSONObject mainObj = new JSONObject();
                    mainObj.put("Latitude", lat);
                    mainObj.put("Longitude", longi);
                    mainObj.put("DriverId", userId);
                    mainObj.put("IsActive", true);
                    mainObj.put("OnlineStatus", false);

                    mainItem.put(mainObj);

                    parent.put("DriverLoc", mainItem);
                } catch (JSONException je) {
                    je.printStackTrace();
                }

//                new UpdateLocationToServer().execute("http://85.194.94.241/oreganoservices/api/DriverApp?Latitude="+lat+"&Longitude="+longi+"&driverid="+userId);

                new UpdateLocationToServer().execute(parent.toString());
                Log.i("parent TAG", "" + parent.toString());
                timer.cancel();
                userPrefEditor.clear();
                userPrefEditor.commit();

                Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginIntent);
                finish();
            } else {
//                                    item.setChecked(false);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, android.R.style.Theme_Material_Light_Dialog));

                // set title
                alertDialogBuilder.setTitle("Oregano");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Please Complete Your Orders first")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
//                                    return false;
            }
            if (dialog != null) {
                dialog.dismiss();
            }
            loading = false;
            super.onPostExecute(result);

        }

    }



    @Override
    protected void onResume() {

        super.onResume();
        Log.e("TAG", "onResume");

        response = userPrefs.getString("user_profile", null);
        if (response != null) {
            Log.e("TAG", "response" + response);
            try {
                Log.e("TAG", "response" + response);
                JSONObject property = new JSONObject(response);
                Log.e("TAG", "response" + response);
                JSONObject userObjuect = property.getJSONObject("profile");

                Log.e("TAG", "response" + property.getJSONObject("profile"));

//                String fullname,familyname;
//                fullname=userObjuect.getString("fullName");
//                familyname=userObjuect.getString("familyName");

//                if(userObjuect.getString("nick_name").length()>0) {
//                    getSupportActionBar().setTitle(userObjuect.getString("nick_name"));
//                }else{
//                    getSupportActionBar().setTitle(""+fullname+" "+familyname);
                getSupportActionBar().setTitle(userObjuect.getString("fullName") + " " + userObjuect.getString("family_name"));
//                }
                Log.e("TAG", "nickname " + userObjuect.getString("family_name") + "" + userObjuect.getString("fullName"));
            } catch (JSONException e) {
                Log.d("", "Error while parsing the results!");
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            }
            else {
                bindService(new Intent(this, GPSTracker.class), mServiceConnection,
                        Context.BIND_AUTO_CREATE);
            }
        }
        else {
            bindService(new Intent(this, GPSTracker.class), mServiceConnection,
                    Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }

        super.onStop();
    }
}
