package com.cs.oreganodriver;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;

/**
 * Created by CS on 12-08-2016.
 */
public class StoreDetails extends AppCompatActivity {

    TextView navUser, viewUser, storeName, storeAddress, orderTakenTxt, orderNumber;
    LinearLayout navStore, call, message, orderTaken;
    Order order;

    private static final String[] PHONE_PERMS = {
            android.Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;
    SharedPreferences userPrefs;
    String userId, response;

    private Timer timer = new Timer();

    public static Double lat, longi;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 4;

    int traveltime1, traveltime2, traveltime3, traveltime;

    String URL_DISTANCE = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.store_details);

        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            }
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        order = (Order) getIntent().getSerializableExtra("order_obj");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navUser = (TextView) findViewById(R.id.navigate_user);
        viewUser = (TextView) findViewById(R.id.view_user);
        navStore = (LinearLayout) findViewById(R.id.navigate_store);
        storeName = (TextView) findViewById(R.id.store_detail_name);
        storeAddress = (TextView) findViewById(R.id.store_detail_address);
        call = (LinearLayout) findViewById(R.id.store_detail_call);
        message = (LinearLayout) findViewById(R.id.store_detail_message);
        orderTaken = (LinearLayout) findViewById(R.id.order_taken);
        orderTakenTxt = (TextView) findViewById(R.id.order_taken_txt);
        orderNumber = (TextView) findViewById(R.id.order_number);

        storeName.setText(order.getStoreName());
        storeAddress.setText(WordUtils.capitalizeFully(order.getStoreAddress()));
        orderNumber.setText("#" + order.getInvoiceNo());

        navUser.setVisibility(View.GONE);
        viewUser.setVisibility(View.GONE);

        navUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + order.getUserLat() + "," + order.getUserLong());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

            }
        });

        viewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(StoreDetails.this, CustomerDetails.class);
                i.putExtra("order_obj", order);
                startActivity(i);

            }
        });

        navStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + order.getStoreLat() + "," + order.getStoreLong());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + order.getStoreMobile()));
                        if (ActivityCompat.checkSelfPermission(StoreDetails.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + order.getStoreMobile()));
                    startActivity(intent);
                }
            }
        });

        new getTrafficTime1().execute();

        orderTaken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                traveltime = traveltime1 + traveltime2 + traveltime3;

                Log.i("TAG", "traveltime1 " + traveltime1);
                Log.i("TAG", "traveltime2 " + traveltime2);
                Log.i("TAG", "traveltime3 " + traveltime3);
                Log.i("TAG", "traveltime " + traveltime);

                Date datetime1;
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);

                Calendar datetime = Calendar.getInstance();

                datetime.add(Calendar.MINUTE, traveltime);

                datetime1 = datetime.getTime();

                String datetime2 = dateFormat.format(datetime1);

                new UpdateOrderStatus().execute(Constants.live_url + "/api/DriverApp?DriverId=" + userId + "&OrderId=" + order.getOrderId() + "&OrderStatus=OnTheWay&Comment=");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(StoreDetails.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {

                } else {
                    Toast.makeText(StoreDetails.this, "Location permission denied", Toast.LENGTH_LONG).show();
                }
                break;

            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + order.getStoreMobile()));
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);
                } else {
                    Toast.makeText(StoreDetails.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public class getTrafficTime1 extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        String distanceResponse;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(StoreDetails.this);

        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                distanceResponse = jParser
                        .getJSONFromUrl(URL_DISTANCE + lat + "," + longi + "&destinations=" + order.getStoreLat() + "," + order.getStoreLong() + "&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
                Log.i("TAG", "user response: " + distanceResponse);
                return distanceResponse;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(StoreDetails.this, "Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    try {
                        JSONObject jo = new JSONObject(result);
                        JSONArray ja = jo.getJSONArray("rows");
                        JSONObject jo1 = ja.getJSONObject(0);
                        JSONArray ja1 = jo1.getJSONArray("elements");
                        JSONObject jo2 = ja1.getJSONObject(0);
                        JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
                        String secs = jo3.getString("text");
                        String value = jo3.getString("value");

                        traveltime1 = Integer.parseInt(value) / 60;


//                        if(language.equalsIgnoreCase("En")) {
//                        }else if(language.equalsIgnoreCase("Ar")){
//                            travelTimeText.setText(secs+ "  وقت السفر" );
//                        }
//                        Date current24Date = null, currentServerDate = null;
//                        Date expectedTimeDate = null, expectedTime24 = null;
//                        try {
//                            current24Date = timeFormat.parse(timeResponse);
//                            expectedTime24 = timeFormat4.parse(order.getExpectedTime());
//                            Log.e("TAG", "response" + timeResponse);
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
//                        String currentTime = timeFormat1.format(current24Date);
//                        String expTimeStr = timeFormat4.format(expectedTime24);
//                        try {
//                            currentServerDate = timeFormat1.parse(currentTime);
//                            expectedTimeDate = timeFormat4.parse(expTimeStr);
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
//
//
//                        long diff = expectedTimeDate.getTime() - currentServerDate.getTime();
//
//                        long diffSeconds = diff / 1000 % 60;
//                        long diffMinutes = diff / (60 * 1000) % 60;
//                        long diffHours = diff / (60 * 60 * 1000) % 24;
//                        int expMins = (int) diffMinutes * 60 * 1000;
//                        Log.i("TAG", "mins response: " + expMins);
//                        int mins = (Integer.parseInt(value) / 60) + 1;
//                        Calendar now = Calendar.getInstance();
//                        now.setTime(currentServerDate);
//                        now.add(Calendar.MINUTE, mins);
//                        currentServerDate = now.getTime();
//                        String CTimeString = timeFormat1.format(currentServerDate);
//                        travelTime.setText("Travel Time: " + CTimeString);
//                        int noOfMinutes = mins * 60 * 1000;//Convert minutes into milliseconds
//
//                        startTimer(noOfMinutes);//start countdown
//                        startTimer1(expMins);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                new getTrafficTime2().execute();
            }
            super.onPostExecute(result);

        }

    }

    public class getTrafficTime2 extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        String distanceResponse;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(StoreDetails.this);

        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                distanceResponse = jParser
                        .getJSONFromUrl(URL_DISTANCE + order.getStoreLat() + "," + order.getStoreLong() + "&destinations=" + order.getUserLat() + "," + order.getUserLong() + "&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
                Log.i("TAG", "user response: " + distanceResponse);
                return distanceResponse;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(StoreDetails.this, "Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    try {
                        JSONObject jo = new JSONObject(result);
                        JSONArray ja = jo.getJSONArray("rows");
                        JSONObject jo1 = ja.getJSONObject(0);
                        JSONArray ja1 = jo1.getJSONArray("elements");
                        JSONObject jo2 = ja1.getJSONObject(0);
                        JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
                        String secs = jo3.getString("text");
                        String value = jo3.getString("value");

                        traveltime2 = Integer.parseInt(value) / 60;


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                new getTrafficTime3().execute();
            }
            super.onPostExecute(result);

        }

    }

    public class getTrafficTime3 extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        String distanceResponse;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(StoreDetails.this);

        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                distanceResponse = jParser
                        .getJSONFromUrl(URL_DISTANCE + order.getUserLat() + "," + order.getUserLong() + "&destinations=" + order.getStoreLat() + "," + order.getStoreLong() + "&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
                Log.i("TAG", "user response: " + distanceResponse);
                return distanceResponse;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(StoreDetails.this, "Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    try {
                        JSONObject jo = new JSONObject(result);
                        JSONArray ja = jo.getJSONArray("rows");
                        JSONObject jo1 = ja.getJSONObject(0);
                        JSONArray ja1 = jo1.getJSONArray("elements");
                        JSONObject jo2 = ja1.getJSONObject(0);
                        JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
                        String secs = jo3.getString("text");
                        String value = jo3.getString("value");

                        traveltime3 = Integer.parseInt(value) / 60;


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            super.onPostExecute(result);

        }

    }

    public class UpdateOrderStatus extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(StoreDetails.this);
            dialog = ProgressDialog.show(StoreDetails.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(StoreDetails.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(StoreDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            if (result.contains("Failure")) {
                                JSONObject jo = new JSONObject(result);
                                String msg = jo.getString("Failure");
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(StoreDetails.this, android.R.style.Theme_Material_Light_Dialog));

                                // set title
                                alertDialogBuilder.setTitle("Oregano");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage(msg)
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });

                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();
                            } else {
                                Intent i = new Intent(StoreDetails.this, CustomerDetails.class);
                                i.putExtra("order_obj", order);
                                startActivity(i);
                                finish();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(StoreDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

}
